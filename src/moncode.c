// Source : https://www.tutorialspoint.com/cprogramming/c_error_handling.htm

#include <stdio.h>
#include <stdlib.h>

int main() {

  int dividend = 20;
  int divisor = 5;
  int quotient;

  if( divisor == 0) {
    fprintf(stderr, "Division by zero! Exiting...\n");
    exit(EXIT_FAILURE);
  }

  quotient = dividend / divisor;
  fprintf(stderr, "Value of quotient is : %d\n", quotient );

  exit(EXIT_SUCCESS);
}
